import pyautogui as kp
import random as r
import time as t

def key(typekey):
    kp.press(typekey)
    t.sleep(1)

def activate():
    key('i')
    key('num9')
    t.sleep(5)
    key('num5')
    key('num2')
    key('num2')
    key('num5')
    key('num2')
    key('num5')
    key('num0')
    for i in range(3):
        key('num2')
    key('num5')
    key('num8')
    key('num5')
    for i in range(4):
        key('num2')
    t.sleep(1)
    key('num9')

def exit():
    for i in range(3):
        key('num0')

def return():
    key('num9')
    t.sleep(1)
    for i in range(2):
        key('num5')

def teleport():
    key('num5')
    for i in range(r.randint(1, 25)):
        key('num2')
    key('num5')
